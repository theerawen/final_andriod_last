package com.theeramed.myfinalandriod

import android.graphics.Insets.add
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.graphics.Insets.add
import androidx.core.view.OneShotPreDrawListener.add
import androidx.recyclerview.widget.LinearLayoutManager
import com.theeramed.myfinalandriod.model.Products.ProductsData
import com.theeramed.myfinalandriod.adapter.ProductsAdapter
import kotlinx.android.synthetic.main.activity_main.*




class MainActivity : AppCompatActivity() {
    private lateinit var productsAdapter: ProductsAdapter
    private lateinit var productsData: ArrayList<ProductsData>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        productsData = ArrayList<ProductsData>()
        productsAdapter = ProductsAdapter(productsData)
        productRecycler.layoutManager = LinearLayoutManager(this)
        productRecycler.adapter = productsAdapter

        /** Mock Data*/
        fun listProucts(){
            productsData.add(ProductsData(R.drawable.plus100,"1. 100 PLUS:","13 บาท รสชาติหวาน เปรี้ยว ซ่า คล้ายน้ำอัดลม มีกลิ่นหอม"))
            productsData.add(ProductsData(R.drawable.p2,"2. GATORADE:","25 บาท รสชาติที่คุ้นเคยไม่หวานเกินไป กลิ่นหอมมะนาวเจือจาง เปรี้ยวปลายลิ้น พลังงาน 125 KCL"))
            productsData.add(ProductsData(R.drawable.p3,"3. POWDURANCE:","16 บาท มีรสชาติของผลไม้รวมเบาๆ มีกลิ่มหอมผลไม้ เปรี้ยวอมหวาน"))
            productsData.add(ProductsData(R.drawable.p4,"4. สปอเรต:","10 บาท กินง่ายมาก หวาน หอม คล้ายพวกเจลลี่"))
            productsData.add(ProductsData(R.drawable.p5,"5. POCARI SWEAT:","25 บาท มีรสชาติเจือจาง กลิ่นหอมซิตรัส หวานอ่อนๆ ไม่เปรี้ยว"))
            productsData.add(ProductsData(R.drawable.p6,"6. สปอนเซอร์:","10 บาท กินง่าย หวานมีกลิ่นหอม คนส่วนใหญ่ คุ้นเคยกันดี"))
            productsData.add(ProductsData(R.drawable.p7,"7. เอ็มพลัส (เหลือง):","10 บาท กินแล้วรู้สึกสดชื่น คล้ายไอติมฟรุตตี้ รสชาติดี กินง่ายที่สุด"))
            productsData.add(ProductsData(R.drawable.p8,"8. CARABAO (energy drink):","25 บาท รสชาติเหมือนน้ำอัดลม เปรี้ยวซ่ามาก มีกลิ่นแอปเปิ้ลชัด"))

        }



    }
}