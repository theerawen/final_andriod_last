package com.theeramed.myfinalandriod.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.theeramed.myfinalandriod.R
import com.theeramed.myfinalandriod.model.Products.ProductsData
import org.w3c.dom.Text

class ProductsAdapter (private val productsList:List<ProductsData>): RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder>() {
    inner class ProductsViewHolder(v:View):RecyclerView.ViewHolder(v){
        var productsName = v.findViewById<TextView>(R.id.productsName)
        var productsDetail = v.findViewById<TextView>(R.id.productsDetail)
        var productsImg= v.findViewById<ImageView>(R.id.productsImg)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.product_list,parent,false)
        return ProductsViewHolder(v)
    }

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        val productsList = productsList[position]
        holder.productsName.text = productsList.productsName
        holder.productsDetail.text = productsList.productsDetail
        holder.productsImg.setImageResource(productsList.productsImg)

    }

    override fun getItemCount(): Int {
        return productsList.size
    }
}
